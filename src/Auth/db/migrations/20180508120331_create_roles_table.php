<?php


use Phinx\Migration\AbstractMigration;

class CreateRolesTable extends AbstractMigration
{

    public function change()
    {
        $this->table('roles')
            ->addColumn('prefix', 'string')
            ->addIndex('prefix', ['unique' => true])
            ->create();
    }
}
