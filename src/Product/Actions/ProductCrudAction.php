<?php

namespace App\Product\Actions;

use App\Product\Entity\Product;
use App\Product\Table\ProductTable;
use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class ProductCrudAction extends CrudAction
{
    protected $viewPath = "@product/admin/products";

    protected $routePrefix = "product.admin";

    /**
     * PostCrudAction constructor.
     * @param RendererInterface $renderer
     * @param Router $router
     * @param ClubTable $table
     * @param FlashService $flash
     * @param ClubUpload $clubUpload
     */
    public function __construct(
        RendererInterface $renderer,
        Router $router,
        ProductTable $table,
        FlashService $flash
    ) {
        parent::__construct($renderer, $router, $table, $flash);
    }

    /**
     * @param ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(ServerRequestInterface $request)
    {
        return parent::delete($request);
    }

    protected function getNewEntity()
    {
        $product = new Product();
        return $product;
    }

    /**
     * Filtre des paramètres particuliers
     * @param ServerRequestInterface $request
     * @param Club $club
     * @return array
     */
    protected function getParams(ServerRequestInterface $request, $product) : array
    {
        $params = array_merge($request->getParsedBody(), $request->getUploadedFiles());

        return  array_filter($params, function ($key) {
            return in_array($key, [
                'name',
                'visible',
                'price',
                'contributor_price'
            ]);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Validator
     */
    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        $validator = parent::getValidator($request)
            ->required('name', 'visible', 'price', 'contributor_price')
            ->length('name', 2, 32)
            ->money('price')
            ->money('contributor_price');

        return $validator;
    }
}
