<?php


use Phinx\Seed\AbstractSeed;

class UserSeeder extends AbstractSeed
{

    public function run()
    {
        $users = $this->table('users');

        $this->table('user2role')->dropForeignKey('user_id');
        $users->truncate();

        // ADMIN
        $users->insert([
                'firstname' => 'admin',
                'lastname' => 'admin',
                'email' => 'admin@admin.fr',
                'password' => password_hash('admin', PASSWORD_DEFAULT)
            ])->save();

        // USER
        $users->insert([
            'firstname' => 'John',
            'lastname' => 'Doe',
            'nickname' => 'Joe',
            'email' => 'john@doe.fr',
            'password' => password_hash('0000', PASSWORD_DEFAULT)
        ])->save();

        $users->insert([
            'firstname' => 'Rudel',
            'lastname' => 'Dupont',
            'email' => 'a@b.c',
            'password' => password_hash('0000', PASSWORD_DEFAULT)
        ])->save();

        $this->table('user2role')
            ->addForeignKey('user_id', 'users', 'id', ['delete' => 'CASCADE'])
            ->save();
    }
}
