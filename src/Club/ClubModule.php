<?php

namespace App\Club;

use App\Club\Actions\ClubAction;
use App\Club\Actions\ClubAdminAction;
use App\Club\Actions\ClubAdminMemberAction;
use App\Club\Actions\ClubCrudAction;
use App\Club\Actions\ClubDashboardAction;
use App\Club\Actions\ClubOwnerAction;
use App\Club\Actions\ClubOwnerMemberAction;
use Framework\Router;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;

class ClubModule extends Module
{

    const DEFINITIONS = __DIR__ . '/config.php';

    const MIGRATIONS = __DIR__ . '/db/migrations';

    const SEEDS = __DIR__ . '/db/seeds';

    /**
     * BlogModule constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath('club', __DIR__ . '/views');

        $router = $container->get(Router::class);
        $prefix = $container->get('club.prefix');

        $router->get($prefix, ClubAction::class, 'club.index');
        $router->get(
            $prefix . "/{slug}-{id}",
            ClubAction::class,
            'club.show',
            [
                'slug' => '[a-z\-0-9]+',
                'id' => '[0-9]+'
            ]
        );

        if ($container->has('admin.prefix')) {
            $prefix = $container->get('admin.prefix');

            /* Routes admin bde */
            // Lister les clubs
            $router->get("$prefix/clubs", ClubAdminAction::class, 'club.admin.index');
            // Ajouter un club
            $router->get("$prefix/clubs/creer", ClubAdminAction::class, "club.admin.create");
            $router->post("$prefix/clubs/creer", ClubAdminAction::class);
            // Supprimer le club
            $router->delete("$prefix/clubs/supprimer/{id}", ClubAdminAction::class, "club.admin.delete", [
                "id" => "\d+"
            ]);
            // Toggle l'activation du club
            $router->post("$prefix/clubs/activer/{id}", ClubAdminAction::class, "club.admin.activate", [
                "id" => "\d+"
            ]);

            $router->get("$prefix/clubs/{slug}/membres", ClubAdminMemberAction::class, 'club.admin.member', [
                "id" => '\d+'
            ]);
            $router->post("$prefix/clubs/{slug}/membres", ClubAdminMemberAction::class, null, [
                "id" => '\d+'
            ]);
            $router->delete(
                "$prefix/clubs/{slug}/membres/{id}",
                ClubAdminMemberAction::class,
                'club.admin.member.delete',
                [
                    'slug' => '[a-z]+',
                    "id" => '\d+'
                ]
            );

            /* Routes admin clubs */
            // Dashboard des clubs
            $router->get('/{slug}-admin', ClubDashboardAction::class, 'club.owner.dashboard', [
                'slug' => '[a-z]+'
            ]);
            // Editer le club
            $router->get("/{slug}-admin/editer", ClubOwnerAction::class, 'club.owner.edit', [
                'slug' => '[a-z]+'
            ]);
            $router->post("/{slug}-admin/editer", ClubOwnerAction::class, null, [
                'slug' => '[a-z]+'
            ]);
            // Gestion des membres d'un club par le clubs
            $router->get("/{slug}-admin/membres", ClubOwnerMemberAction::class, 'club.owner.member', [
                'slug' => '[a-z]+'
            ]);
            $router->post("/{slug}-admin/membres", ClubOwnerMemberAction::class, null, [
                'slug' => '[a-z]+'
            ]);
            $router->delete("/{slug}-admin/membres/{id}", ClubOwnerMemberAction::class, 'club.owner.member.delete', [
                'slug' => '[a-z]+', 'id' => "\d+"
            ]);
        }
    }
}
