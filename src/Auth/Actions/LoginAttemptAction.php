<?php

namespace App\Auth\Actions;

use App\Auth\DatabaseAuth;
use App\Auth\NotConfirmedAccountException;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;

class LoginAttemptAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;

    /**
     * @var DatabaseAuth
     */
    private $auth;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var Router
     */
    private $router;

    use RouterAwareAction;

    public function __construct(
        RendererInterface $renderer,
        DatabaseAuth $auth,
        FlashService $flashService,
        Router $router
    ) {
        $this->renderer = $renderer;
        $this->auth = $auth;
        $this->flashService = $flashService;
        $this->router = $router;
    }

    /**
     * @param ServerRequestInterface $request
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request)
    {
        $params = $request->getParsedBody();
        try {
            $user = $this->auth->login($params['email'], $params['password']);
        } catch (NotConfirmedAccountException $e) {
            $this->flashService->error('Veuillez confirmer votre compte grâce à l\'email qui vous à été envoyé.');
            return $this->redirect('auth.login');
        }

        if ($user) {
            return $this->redirect('account.dashboard');
        } else {
            $this->flashService->error('Email ou mot de passe incorrect.');
            return $this->redirect('auth.login');
        }
    }
}
