<?php

namespace App\Club\Actions;

use App\Auth\Table\UserToRoleTable;
use App\Auth\UserTable;
use App\Blog\Table\PostTable;
use App\Club\Table\ClubTable;
use Framework\Database\NoRecordException;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface;

class ClubDashboardAction
{
    private $renderer;
    /**
     * @var ClubTable
     */
    private $clubTable;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var PostTable
     */
    private $postTable;
    /**
     * @var UserTable
     */
    private $userTable;

    public function __construct(
        RendererInterface $renderer,
        ClubTable $clubTable,
        FlashService $flashService,
        PostTable $postTable,
        UserTable $userTable
    ) {
        $this->renderer = $renderer;
        $this->clubTable = $clubTable;
        $this->flashService = $flashService;
        $this->postTable = $postTable;
        $this->userTable = $userTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        try {
            $club = $this->clubTable->findBy('slug', $request->getAttribute('slug'));
        } catch (NoRecordException $e) {
            $this->flashService->error('Ce club n\'existe pas.');
            return new RedirectResponse('/espaceZZ');
        }

        $nbPosts = $this->postTable->getClubPostsNumber($club->slug);
        $waitingPosts = $this->postTable->getClubUnvalidatedPostsNumber($club->slug);
        $nbMembres = $this->userTable->getClubMembersNumber($club->slug);
        return $this->renderer->render(
            '@club/admin/owner/dashboard',
            compact('club', 'nbPosts', 'waitingPosts', 'nbMembres')
        );
    }
}
