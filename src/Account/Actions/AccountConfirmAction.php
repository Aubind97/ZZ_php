<?php

namespace App\Account\Actions;

use App\Account\Table\UserTable;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class AccountConfirmAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var UserTable
     */
    private $userTable;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var Router
     */
    private $router;

    public function __construct(
        RendererInterface $renderer,
        UserTable $userTable,
        FlashService $flashService,
        Router $router
    ) {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
        $this->flashService = $flashService;
        $this->router = $router;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        $user = $this->userTable->find($request->getAttribute('id'));

        if ($user !== null &&
            $user->getConfirmedToken() === $request->getAttribute('token') &&
            $user->getConfirmedAt() === null
        ) {
            if ($request->getMethod() === 'GET') {
                return $this->renderer->render('@account/confirm');
            } else {
                $params = $request->getParsedBody();
                $validator = (new Validator($params))
                    ->length('password', 4)
                    ->confirm('password');

                if ($validator->isValid()) {
                    $this->userTable->confirm($user->getId());
                    $this->userTable->update($user->getId(), [
                        'password' => password_hash($params['password'], PASSWORD_DEFAULT)
                    ]);
                    $this->flashService->success('Votre mot de passe a bien été enregistré');
                    return new RedirectResponse($this->router->generateUri('auth.login'));
                } else {
                    $errors = $validator->getErrors();
                    return $this->renderer->render('@auth/reset', compact('errors'));
                }
            }
        } else {
            $this->flashService->error("Votre token est invalide ou votre compte est déjà activé");
            return new RedirectResponse('/');
        }
    }
}
