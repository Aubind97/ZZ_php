<?php

namespace App\Auth\Table;

use Framework\Database\Table;

class UserToRoleTable extends Table
{
    protected $table = 'user2role';
}
