<?php

namespace App\Account;

use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

class AccountWidget implements WidgetInterface
{

    private $renderer;

    public function __construct(RendererInterface $renderer)
    {
        $this->renderer = $renderer;
    }

    public function render(): string
    {
        return $this->renderer->render('@account/widget');
    }
}
