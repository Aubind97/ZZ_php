<?php

namespace Framework;

use Framework\Database\Table;
use Framework\Validator\ValidationError;
use Psr\Http\Message\UploadedFileInterface;

class Validator
{
    private const MIME_TYPES = [
        'jpg' => 'image/jpeg',
        'png' => 'image/png'
    ];

    private $params;

    private $errors = [];

    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * Vérifie la validité d'une date
     * @param string $key
     * @param string $format
     * @return Validator
     */
    public function dateTime(string $key, $format = 'Y-m-d H:i:s') : self
    {
        $value = $this->getValue($key);
        $date = \DateTime::createFromFormat($format, $value);
        $error = \DateTime::getLastErrors();
        if ($error['error_count'] > 0 || $error['warning_count'] > 0 || $date === false) {
            $this->addError($key, 'datetime', [$format]);
        }
        return $this;
    }

    /**
     * Verifie la validiter de la taille d'un champ
     * @param string $key
     * @param int|null $min
     * @param int|null $max
     * @return Validator
     */
    public function length(string $key, ?int $min, ?int $max = null) : self
    {
        $value = $this->getValue($key);
        $length = mb_strlen($value);
        if (!is_null($min) &&
            !is_null($max) &&
            ($length < $min || $length > $max)
        ) {
            $this->addError($key, 'betweenLength', [$min, $max]);
        }

        if (!is_null($min) &&
            $length < $min
        ) {
            $this->addError($key, 'minLength', [$min]);
        }

        if (!is_null($max) &&
            $length > $max
        ) {
            $this->addError($key, 'maxLength', [$max]);
        }

        return $this;
    }

    /**
     * Vérifie qu'une chaine est un slug
     * @param string $key
     * @return Validator
     */
    public function slug(string $key) : self
    {
        $value = $this->getValue($key);
        $pattern = '/^[a-z0-9]+(-?[a-z0-9]+)*$/';
        if (!is_null($value) && !preg_match($pattern, $value)) {
            $this->addError($key, 'slug');
        }
        return $this;
    }

    public function money($key) : self
    {
        $value = $this->getValue($key);
        $pattern = '/^[0-9]*((.|,)[0-9]{1,2})?$/';
        if (!is_null($value) && !preg_match($pattern, $value)) {
            $this->addError($key, 'money');
        }
        return $this;
    }
    /**
     * Vérifie si le fichier a bien été uploader
     * @param string $key
     * @return Validator
     */
    public function uploaded(string $key) : self
    {
        /** @var UploadedFileInterface $file */
        $file = $this->getValue($key);
        if ($file === null || $file->getError() !== UPLOAD_ERR_OK) {
            $this->addError($key, 'uploaded');
        }
        return $this;
    }

    /**
     * Vérifie le format d'un fichier
     * @param string $key
     * @param array $extensions
     * @return Validator
     */
    public function extension(string $key, array $extensions) : self
    {
        /** @var UploadedFileInterface $file */
        $file = $this->getValue($key);
        if ($file !== null && $file->getError() === UPLOAD_ERR_OK) {
            $type = $file->getClientMediaType();
            $extension = strtolower(pathinfo($file->getClientFilename(), PATHINFO_EXTENSION));
            $expectedType = self::MIME_TYPES[$extension] ?? null;
            if (!in_array($extension, $extensions) || $expectedType !== $type) {
                $this->addError($key, 'filetype', [join(',', $extensions)]);
            }
        }

        return $this;
    }

    public function maxSize(string $key, int $maxSize) : self
    {
        $file = $this->getValue($key);
        if ($file !== null && $file->getError() === UPLOAD_ERR_OK) {
            $size = $file->getSize();
            if ($size > $maxSize) {
                $this->addError($key, 'filesize', [$maxSize]);
            }
        }

        return $this;
    }

    /**
     * Vérifie que les champs sont présent dans le tableau
     * @param string ...$keys
     * @return Validator
     */
    public function required(string ...$keys) : self
    {
        foreach ($keys as $key) {
            $value = $this->getValue($key);
            if (is_null($value)) {
                $this->addError($key, 'required');
            }
        }
        return $this;
    }

    /**
     * Verifie que le champ n'est pas vide
     * @param string ...$keys
     * @return Validator
     */
    public function notEmpty(string ...$keys) : self
    {
        foreach ($keys as $key) {
            $value = $this->getValue($key);
            if (is_null($value) || empty($value)) {
                $this->addError($key, 'empty');
            }
        }
        return $this;
    }

    /**
     * Verifie que le champ est un email valide
     * @param string $key
     * @return Validator
     */
    public function email(string $key) : self
    {
        $value = $this->getValue($key);
        if (filter_var($value, FILTER_VALIDATE_EMAIL) === false) {
            $this->addError($key, 'email');
        }
        return $this;
    }

    public function unique(string $key, $table, ?\PDO $pdo = null, ?int $exclude = null) : self
    {
        if ($table instanceof Table) {
            $pdo = $table->getPdo();
            $table = $table->getTable();
        }
        $value = $this->getValue($key);
        $query = "SELECT id FROM $table WHERE $key = ?";
        $params = [$value];
        if ($exclude !== null) {
            $query .= " AND id != ?";
            $params[] = $exclude;
        }
        $statement = $pdo->prepare($query);
        $statement->execute($params);
        if ($statement->fetchColumn() !== false) {
            $this->addError($key, 'unique', [$value]);
        }
        return $this;
    }

    public function exists(string $field, string $key, string $table, \PDO $pdo): self
    {
        $value = $this->getValue($key);
        $statement = $pdo->prepare("SELECT id FROM $table WHERE $field = ?");
        $statement->execute([$value]);
        if ($statement->fetchColumn() === false) {
            $this->addError($key, 'exists', [$table]);
        }
        return $this;
    }

    /**
     * Vérifie que le champ est correctement confrimé
     * @param string $key
     * @return Validator
     */
    public function confirm(string $key) : self
    {
        $value = $this->getValue($key);
        $valueConfrim = $this->getValue($key . '_confirm');

        if ($value !== $valueConfrim) {
            $this->addError($key, 'confirm');
        }

        return $this;
    }



    public function isValid() : bool
    {
        return empty($this->errors);
    }

    /**
     * Récupère les erreurs
     * @return ValidationError[]
     */
    public function getErrors() : array
    {
        return $this->errors;
    }

    /**
     * Ajoute une errueur
     * @param string $key
     * @param string $rule
     * @param array $attributes
     */
    private function addError(string $key, string $rule, array $attributes = []) : void
    {
        $this->errors[$key] = new ValidationError($key, $rule, $attributes);
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    private function getValue(string $key)
    {
        if (array_key_exists($key, $this->params)) {
            return $this->params[$key];
        }
        return null;
    }
}
