<?php


use Phinx\Seed\AbstractSeed;

class ClubsSeeder extends AbstractSeed
{

    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create('fr_FR');
        $clubs = $this->table('clubs');

        $this->table('posts')->dropForeignKey('created_by');
        // Vide la table pour éviter les duplications
        $clubs->truncate();

        $clubNames = ['isibouffe', 'pixel', 'rezzo', 'isibot'];

        for ($i = 0; $i < sizeof($clubNames); $i++) {
            $date = $faker->unixTime('now');
            $active = $i % 2 == 0 ? 1 : 0;
            $data[] = [
                'name' => $clubNames[$i],
                'slug' => strtolower($clubNames[$i]),
                'description' => $faker->text(3000),
                'short_description' => $faker->text(125),
                'is_active' => $active,
                'created_at' => date('Y-m-d H:i:s', $date)
            ];
        }

        $clubs->insert($data)->save();
        $this->table('posts')->addForeignKey('created_by', 'clubs', 'id');
    }
}
