<?php
/**
 * Created by PhpStorm.
 * User: laurent
 * Date: 4/27/18
 * Time: 8:15 PM
 */

namespace App\Product\Actions;

use App\Product\Table\ProductTable;
use Framework\Renderer\RendererInterface;
use Framework\Response\RedirectResponse;
use Framework\Router;
use Framework\Session\FlashService;
use Psr\Http\Message\ServerRequestInterface as Request;

class ProductAction
{

    private $renderer;

    private $router;
    /**
     * @var ProductTable
     */
    private $productTable;
    /**
     * @var FlashService
     */
    private $flashService;

    private $messages = [
       'activate' => "Le produit '%s' a bien été  rendu %s."
    ];

    public function __construct(
        RendererInterface $renderer,
        Router $router,
        ProductTable $productTable,
        FlashService $flashService
    ) {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->productTable = $productTable;
        $this->flashService = $flashService;
    }

    public function __invoke(Request $request)
    {
        if ($request->getMethod() === 'POST') {
            return $this->activate($request);
        }
        $items = $this->productTable->findVisible()->fetchAll();
        return $this->renderer->render('@product/index', compact('items'));
    }

    private function activate(Request $request)
    {
        $productId = $request->getAttribute('id');
        $product = $this->productTable->find($productId);

        $this->productTable->update($productId, ['visible' => 1 - $product->visible]);

        $this->flashService->success(
            sprintf($this->messages['activate'], $product->name, $product->visible ? 'invisible' : 'visible')
        );

        return new RedirectResponse($this->router->generateUri('product.admin.index'));
    }
}
