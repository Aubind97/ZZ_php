<?php

use Phinx\Seed\AbstractSeed;

class ProductsSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create('fr_FR');
        $products = $this->table('products');

        $products->truncate();

        for ($i = 0; $i < 15; $i++) {
            $visible = $i % 2 == 0 ? 1 : 0;
            $data[] = [
                'name' => $faker->word,
                'price' => $faker->randomNumber(3)/100,
                'contributor_price' => $faker->randomNumber(3)/100,
                'visible' => $visible
            ];
        }
        $products->insert($data)->save();
    }
}
