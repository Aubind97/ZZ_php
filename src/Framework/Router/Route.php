<?php

namespace Framework\Router;

class Route
{

    /**
     * Nom de la route
     * @var string
     */
    private $name;

    /**
     * Fonction appeler pour cette route
     */
    private $callback;

    /**
     * Paramètres à utiliser pour cette route
     * @var array
     */
    private $params;

    /**
     * Route constructor.
     * @param string $name
     * @param $callback
     * @param array $params
     */
    public function __construct(string $name, $callback, array $params)
    {
        $this->name = $name;
        $this->callback = $callback;
        $this->params = $params;
    }

    /**
     * Récupère le nom d'une route
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * Récupère la fonction a appeler pour cette route
     * @return mixed
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * Récupère les paramètres utilisé pour cette route
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }
}
