<?php

namespace App\Blog\Actions;

use App\Blog\Table\PostTable;
use Framework\Actions\RouterAwareAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ServerRequestInterface;

class BlogAction
{

    private $renderer;

    private $router;

    private $postTable;

    use RouterAwareAction;

    public function __construct(RendererInterface $renderer, Router $router, PostTable $postTable)
    {
        $this->renderer = $renderer;
        $this->router = $router;
        $this->postTable = $postTable;
    }

    public function __invoke(Request $request)
    {
        if ($request->getAttribute('id')) {
            return $this->show($request);
        } else {
            return $this->index($request);
        }
    }

    public function index(Request $request) : string
    {
        $params = $request->getQueryParams();

        $posts = $this->postTable->makeQuery()
            ->select('
                p.id,
                p.name,
                p.content,
                p.slug,
                p.created_at as createdAt,
                p.is_validated as isValidated,
                clubs.logo as logo,
                clubs.name as clubName
            ')
            ->from('posts', 'p')
            ->join('clubs', "clubs.id = p.created_by", 'right')
            ->order("p.created_at DESC")
            ->where("p.is_validated = 1", 'p.created_at <= NOW()', 'clubs.is_active = 1')
            ->paginate(12, $params['p'] ?? 1);

        return $this->renderer->render('@blog/index', compact('posts'));
    }

    public function show(Request $request)
    {
        $slug = $request->getAttribute('slug');
        $post = $this->postTable->findById($request->getAttribute('id'));
        // Si le slug n'est pas celui attendu, on redirige vers la page avec le bon slug
        if ($post->slug != $slug) {
            return $this->redirect('blog.show', [
                'slug' => $post->slug,
                'id' => $post->id
            ]);
        }
        return $this->renderer->render('@blog/show', [
            'post' => $post
        ]);
    }
}
