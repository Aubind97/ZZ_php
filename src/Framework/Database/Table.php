<?php

namespace Framework\Database;

use Pagerfanta\Pagerfanta;

class Table
{
    /**
     * @var \PDO
     */
    protected $pdo;

    /**
     * Nom de la table en BDD
     * @var string
     */
    protected $table = \stdClass::class;

    /**
     * Entité à utilisé
     * @var string|null
     */
    protected $entity;

    /**
     * Table constructor.
     * @param \PDO $pdo
     */
    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function makeQuery() : Query
    {
        return (new Query($this->pdo))
            ->from($this->table, $this->table[0])
            ->into($this->entity);
    }


    /**
     * @param int $id
     * @return bool|mixed
     * @throws NoRecordException
     */
    public function find(int $id)
    {
        return $this->makeQuery()->where("id = $id")->fetchOrFail();
    }

    /**
     * @param string $field
     * @param string $value
     * @return bool|mixed
     * @throws NoRecordException
     */
    public function findBy(string $field, string $value)
    {
        return $this->makeQuery()->where("$field = :field")->params(["field" => $value])->fetchOrFail();
    }


    /**
     * @return Query
     */
    public function findAll() : Query
    {
        return $this->makeQuery();
    }

    /**
     * Met à jour un enregistrement
     * (les params sont envoyer en présisant les clés !)
     * @param int $id
     * @param array $params
     * @return bool
     */
    public function update(int $id, array $params) : bool
    {
        $fieldsQuery = $this->buildFieldQuery($params);

        $params["id"] = $id;
        $statement = $this->pdo->prepare("UPDATE {$this->table} SET $fieldsQuery WHERE id = :id");
        return $statement->execute($params);
    }

    /**
     * Créer un nouveau enregistrement
     * @param array $params
     * @return bool
     */
    public function insert(array $params) : bool
    {
        $fieldsQuery = $this->buildFieldQuery($params);
        $statement = $this->pdo->prepare("INSERT INTO {$this->table} SET $fieldsQuery");
        return $statement->execute($params);
    }

    /**
     * Supprime un enregistrement
     * @param int $id
     * @return bool
     */
    public function delete(int $id) : bool
    {
        $statement = $this->pdo->prepare("DELETE FROM $this->table WHERE id = ?");
        return $statement->execute([$id]);
    }

    /**
     * @param array $params
     * @return string
     */
    private function buildFieldQuery(array $params)
    {
        return join(', ', array_map(function ($field) {
            return "$field = :$field";
        }, array_keys($params)));
    }

    /**
     * @return string
     */
    public function getEntity(): string
    {
        return $this->entity;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @return \PDO
     */
    public function getPdo(): \PDO
    {
        return $this->pdo;
    }
}
