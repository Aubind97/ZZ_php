<?php

namespace Framework\Validator;

class ValidationError
{
    private $key;
    private $rule;
    private $attributes;

    private $message = [
        'required' => 'Le champs %s est requis',
        'empty' => 'Le champs %s ne peut être vide',
        'slug' => 'Le champs %s n\'est pas un slug valide',
        'money' => 'Le champs %s ne correspond pas à un montant d\'argent',
        'email' => 'Le champs %s n\'est pas un email valide',
        'unique' => 'Le champs %s est déjà utilisé par un autre utilisteur',
        'exists' => 'Le champs %s ne correspond à aucune adresse d\'utilisateur ',
        'confirm' => 'Vous n\'avez pas confrimé le champ %s',
        'maxLength' => 'Le champs %s doit contenir moins de %d caractères',
        'minLength' => 'Le champs %s doit contenir plus de %d caractères',
        'betweenLength' => 'Le champs %s doit contenir entre %d et %d caractères',
        'datetime' => 'Le champs %s doit être une date valide (%s)',
        'filetype' => 'Le champs %s n\'est pas au format valide (%s)',
        'uploaded' => 'Vous devez uploader un fichier',
        'filesize' => 'Le %s doit être plus petit que %d octet'
    ];

    public function __construct(string $key, string $rule, array $attributes = [])
    {
        $this->key = $key;
        $this->rule = $rule;
        $this->attributes = $attributes;
    }

    public function __toString()
    {
        $params = array_merge([$this->message[$this->rule], $this->key], $this->attributes);
        return (string)call_user_func_array('sprintf', $params);
    }
}
