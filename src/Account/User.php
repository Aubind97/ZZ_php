<?php

namespace App\Account;

class User extends \App\Auth\User
{
    /**
     * @var string
     */
    private $nickname;

    private $confirmedToken;

    private $confirmedAt;

    /**
     * @return string
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param null|string $nickname
     */
    public function setNickname(?string $nickname): void
    {
        $this->nickname = $nickname;
    }

    /**
     * @return mixed
     */
    public function getConfirmedToken()
    {
        return $this->confirmedToken;
    }

    /**
     * @param mixed $confrimedToken
     */
    public function setConfirmedToken($confrimedToken): void
    {
        $this->confirmedToken = $confrimedToken;
    }

    /**
     * @return mixed
     */
    public function getConfirmedAt()
    {
        return $this->confirmedAt;
    }

    /**
     * @param mixed $confirmedAt
     */
    public function setConfirmedAt($confirmedAt): void
    {
        $this->confirmedAt = $confirmedAt;
    }
}
