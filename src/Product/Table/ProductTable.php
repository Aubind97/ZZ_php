<?php

namespace App\Product\Table;

use App\Product\Entity\Product;
use Framework\Database\Table;

class ProductTable extends Table
{
    protected $entity = Product::class;

    protected $table = 'products';

    public function findVisible()
    {
        return $this->makeQuery()->where("visible = true");
    }

    public function getProductsNumber()
    {
        return $this->makeQuery()->count();
    }

    public function getVisibleProductsNumber()
    {
        return $this->makeQuery()->where('visible = 1')->count();
    }
}
