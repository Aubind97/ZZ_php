<?php

namespace Framework\Renderer;

use Psr\Container\ContainerInterface;

/**
 * Class TwigRendererFactory
 * @package Framework\Renderer
 */
class TwigRendererFactory
{
    /**
     * Initialise le Twig Renderer avec toutes la configuration
     * @param ContainerInterface $container
     * @return TwigRenderer
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : TwigRenderer
    {
        $debug = $container->get('env') !== 'production';

        $viewPath = $container->get('views.path');
        $loader = new \Twig_Loader_Filesystem($viewPath);
        $twig = new \Twig_Environment($loader, [
            'debug' => $debug,
            'cache' => $debug ? false :'tmp/views',
            'auto_reload' => $debug
        ]);

        if ($debug) {
            $twig->addExtension(new \Twig_Extension_Debug());
        }

        if ($container->has('twig.extensions')) {
            foreach ($container->get('twig.extensions') as $extension) {
                $twig->addExtension($extension);
            }
        }
        return new TwigRenderer($twig);
    }
}
