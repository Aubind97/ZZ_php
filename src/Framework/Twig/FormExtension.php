<?php

namespace Framework\Twig;

class FormExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('field', [$this, 'field'], [
                'is_safe' => ['html'],
                'needs_context' => true
            ])
        ];
    }

    public function field(array $context, string $key, $value, string $label, array $options = []) : string
    {
        // On récupère le type
        $type = $options['type'] ?? 'text';
        // On récupère les erreurs
        $error = $this->getErrorHTML($context, $key);
        // On créer des 'buffer' de class
        $class = 'form-group';
        // On convertie si necessaire (ex: date)
        $value = $this->convertValue($value);
        // On répuère les atributs
        $attributes = [
            'class' => trim('form-control ' . ($options['class'] ?? '')),
            'name' => $key,
            'id' => $key
        ];

        // Si il y a des erreur on ajoute les class pour les affichers
        if ($error) {
            $class .= ' has-danger';
            $attributes['class'] .= ' is-invalid';
        }

        // Switch des types d'input
        if ($type === 'textarea') {
            $input = $this->textarea($value, $attributes);
        } elseif ($type === 'checkbox') {
            $input = $this->checkbox($value, $attributes);
        } elseif ($type === 'file') {
            $input = $this->file($attributes);
        } elseif ($type === 'email') {
            $input = $this->email($value, $attributes);
        } elseif ($type === 'password') {
            $input = $this->password($value, $attributes);
        } else {
            $input = $this->input($value, $attributes);
        }

        // On retourn le group normal
        return "
            <div class=\"" . $class . "\">
                <label for=\"{$key}\">{$label}</label>
                {$input}
                {$error}
            </div>
        ";
    }

    private function checkbox(?string $value, array $attributes): string
    {
        $html = '<input type="hidden" name="' . $attributes['name'] . '" value="0"/>';
        if ($value) {
            $attributes['checked'] = true;
        }
        return $html . "<input type=\"checkbox\" " . $this->getHtmlFromArray($attributes) . " value=\"1\">";
    }

    private function file($attributes)
    {
        return "<input type=\"file\" ". $this->getHtmlFromArray($attributes) . ">";
    }

    private function input(?string $value, array $attributes) : string
    {
        return "<input type=\"text\" ". $this->getHtmlFromArray($attributes) ." value=\"{$value}\" >";
    }

    private function password(?string $value, array $attributes) : string
    {
        return "<input type=\"password\" ". $this->getHtmlFromArray($attributes) ." value=\"{$value}\" >";
    }

    private function email(?string $value, array $attributes) : string
    {
        return "<input type=\"email\" ". $this->getHtmlFromArray($attributes) ." value=\"{$value}\" >";
    }

    private function textarea(?string $value, array $attributes) : string
    {
        return "<textarea ". $this->getHtmlFromArray($attributes) ." >{$value}</textarea>";
    }

    private function getErrorHTML($context, $key)
    {
        $error = $context['errors'][$key] ?? false;
        if ($error) {
            return "<small class=\"form-text invalid-feedback\">{$error}</small>";
        }
        return "";
    }

    private function getHtmlFromArray(array $attributes)
    {
        return implode(' ', array_map(function ($key, $value) {
            return "$key=\"$value\"";
        }, array_keys($attributes), $attributes));
    }

    private function convertValue($value) : string
    {
        if ($value instanceof \DateTime) {
            return $value->format('Y-m-d H:i:s');
        }
        return (string)$value;
    }
}
