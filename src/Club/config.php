<?php

/**
 * Configuration des dépendances du module pour le conteneur d'injection
 */

use App\Club\ClubAdminWidget;
use App\Club\ClubOwnerWidget;

return [
    'club.prefix' => '/clubs',
    'account.widgets' => \DI\add([
        \DI\get(ClubOwnerWidget::class)
    ]),
    'admin.widgets' => \DI\add([
        \DI\get(ClubAdminWidget::class)
    ]),

    'public.nav' => \DI\add([
        'club' => [
            'link' => '/clubs',
            'name' => 'Clubs'
        ]
    ])
];
