<?php

use App\Auth\AuthTwigExtension;
use App\Auth\DatabaseAuth;
use App\Auth\ForbiddenMiddleware;
use App\Auth\Mailer\PasswordResetMailer;
use App\Auth\UserTable;
use function DI\add;
use function DI\autowire;
use function DI\get;
use Framework\Auth;

return [
    'auth.login' => '/login',
    'auth.entity' => \App\Auth\User::class,
    'twig.extensions' => add([
        get(AuthTwigExtension::class),
    ]),
    Auth::class => get(DatabaseAuth::class),
    UserTable::class => autowire()->constructorParameter('entity', get('auth.entity')),
    ForbiddenMiddleware::class => autowire()->constructorParameter('redirectPath', get('auth.login')),
    PasswordResetMailer::class => autowire()->constructorParameter('from', get('mail.from'))
];
