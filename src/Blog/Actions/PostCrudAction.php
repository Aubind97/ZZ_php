<?php

namespace App\Blog\Actions;

use App\Blog\Entity\Post;
use App\Blog\Table\PostTable;
use Framework\Actions\CrudAction;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class PostCrudAction extends CrudAction
{
    protected $viewPath = "@blog/admin/posts";

    protected $routePrefix = "blog.admin";

    /**
     * PostCrudAction constructor.
     * @param RendererInterface $renderer
     * @param Router $router
     * @param PostTable $table
     * @param FlashService $flash
     */
    public function __construct(RendererInterface $renderer, Router $router, PostTable $table, FlashService $flash)
    {
        parent::__construct($renderer, $router, $table, $flash);
    }

    protected function getNewEntity()
    {
        $post = new Post();
        $post->createdAt = new \DateTime();
        return $post;
    }

    public function index(ServerRequestInterface $request): string
    {
        $params = $request->getQueryParams();

        $items = $this->table->makeQuery()
            ->select('p.id, p.name, p.content, p.is_validated as isValidated, clubs.name as clubName')
            ->from('posts', 'p')
            ->join('clubs', "clubs.id = p.created_by")
            ->order("p.is_validated ASC, p.created_by DESC")
            ->paginate(12, $params['p'] ?? 1);

        return $this->renderer->render($this->viewPath . '/index', compact('items'));
    }

    /**
     * Filtre des paramètres particuliers
     * @param ServerRequestInterface $request
     * @param $post
     * @return array
     */
    protected function getParams(ServerRequestInterface $request, $post) : array
    {
        $params = array_filter($request->getParsedBody(), function ($key) {
            return in_array($key, ['name', 'content', 'slug', 'created_at']);
        }, ARRAY_FILTER_USE_KEY);

        return array_merge($params, [
           'updated_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     * @return Validator
     */
    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        return (parent::getValidator($request))
            ->required('content', 'name', 'slug', 'created_at')
            ->length('content', 10)
            ->length('name', 2, 250)
            ->length('slug', 2, 50)
            ->dateTime('created_at')
            ->slug('slug');
    }
}
