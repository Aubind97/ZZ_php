<?php

namespace Framework\Twig;

use Framework\Middleware\CsrfMiddleware;
use Framework\Router;

class ModalExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('modal', [$this,'modal'], [
                'is_safe' => ['html'],
            ])
        ];
    }

    public function modal(string $modalId, string $confirmationWord)
    {
        $html = "<div class=\"modal fade\" id=\"$modalId\" 
                      tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"exampleModalCenterTitle\" aria-hidden=\"true\">
                  <div class=\"modal-dialog modal-dialog-centered\" role=\"document\">
                    <div class=\"modal-content\">
                      <div class=\"modal-header\">
                        <h5 class=\"modal-title\" id=\"exampleModalCenterTitle\">Confirmation</h5>
                        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                          <span aria-hidden=\"true\">&times;</span>
                        </button>
                      </div>
                      <div class=\"modal-body\">
                            Veuillez entrer 
                            <strong class=\"text-danger\">$confirmationWord</strong> 
                            pour confirmer cette action
                        <input type='text' class='form-control mt-4' id=\"{$modalId}Input\">
                      </div>
                      <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Annuler</button>
                        <button type=\"button\" class=\"btn btn-danger\" id=\"confirmButton\">Confirmer</button>
                      </div>
                    </div>
                  </div>
                </div>";
        $html .= "<script>
                    var buttons = document.querySelectorAll('.deleteButton')
                    var input = document.querySelector('#{$modalId}Input')
                    var confirmButton = document.querySelector('#confirmButton')
                    var modal = $('.modal')
                    buttons.forEach(function (button) {
                        button.addEventListener('click', function(event){
                            event.preventDefault()
                            input.value = ''

                            confirmButton.addEventListener('click', function(event) {
                                if(input.value === '$confirmationWord') {
                                    button.parentElement.submit()
                                    modal.modal('toggle')
                                }
                            })
                        })
                    })
                    
                    </script>";


        return $html;
    }
}
