<?php

namespace App\Blog\Actions;

use App\Blog\Table\PostTable;
use App\Club\Table\ClubTable;
use Framework\Renderer\RendererInterface;
use Psr\Http\Message\ServerRequestInterface;

class PostOwnerAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var PostTable
     */
    private $postTable;
    /**
     * @var ClubTable
     */
    private $clubTable;

    public function __construct(RendererInterface $renderer, PostTable $postTable, ClubTable $clubTable)
    {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
        $this->clubTable = $clubTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        return $this->index($request);
    }

    private function index(ServerRequestInterface $request)
    {
        $slug = $request->getAttribute('slug');
        $clubId = $this->clubTable->findBy('slug', $slug)->id;
        $params = $request->getQueryParams();

        $posts = $this->postTable->makeQuery()
            ->select('
                p.id,
                p.name,
                p.content,
                p.created_at,
                p.slug,
                p.is_validated as isValidated,
                clubs.name as clubName,
                clubs.logo as logo
            ')
            ->from('posts', 'p')
            ->join('clubs', "clubs.id = p.created_by")
            ->order("p.is_validated ASC, p.created_at DESC")
            ->where("p.created_by = $clubId")
            ->paginate(12, $params['p'] ?? 1);

        return $this->renderer->render('@blog/owner/posts/index', compact('posts', 'slug'));
    }
}
