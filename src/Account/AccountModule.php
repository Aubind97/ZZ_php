<?php

namespace App\Account;

use App\Account\Actions\AccountAction;
use App\Account\Actions\AccountConfirmAction;
use App\Account\Actions\AccountCrudAction;
use App\Account\Actions\AccountEditAction;
use App\Account\Actions\DashboardAction;
use Framework\Auth\LoggedInMiddleware;
use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Container\ContainerInterface;

class AccountModule extends Module
{
    const DEFINITIONS = __DIR__ . '/config.php';

    const MIGRATIONS = __DIR__ . '/db/migrations';

    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath('account', __DIR__ . '/views');
        $router = $container->get(Router::class);
        $prefix = $container->get('account.prefix');

        // Tableau de controle
        $router->get($prefix, [LoggedInMiddleware::class, DashboardAction::class], 'account.dashboard');
        // Page de profil
        $router->get("$prefix/profil", [LoggedInMiddleware::class, AccountAction::class], 'account.profile');
        $router->post("$prefix/profil", [LoggedInMiddleware::class, AccountEditAction::class]);
        // Confirmation du compte
        $router->get("/confirm/{id}/{token}", AccountConfirmAction::class, 'account.confirm', ['id' => '\d+']);
        $router->post("/confirm/{id}/{token}", AccountConfirmAction::class, null, ['id' => '\d+']);

        if ($container->has('admin.prefix')) {
            $prefix = $container->get('admin.prefix');
            // CRUD pour l'admin
            $router->crud("$prefix/account", AccountCrudAction::class, 'account.admin');
        }
    }
}
