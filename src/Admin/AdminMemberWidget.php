<?php

namespace App\Admin;

use App\Auth\Table\RoleTable;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

/**
 * Permet d'aller vers la gestion des clubs
 * Class ClubAdminWidget
 * @package App\Club
 */
class AdminMemberWidget implements WidgetInterface
{
    private $renderer;

    public function __construct(
        RendererInterface $renderer
    ) {
        $this->renderer = $renderer;
    }

    public function render(): string
    {
        return $this->renderer->render('@admin/widgetMember');
    }
}
