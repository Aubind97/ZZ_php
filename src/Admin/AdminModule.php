<?php

namespace App\Admin;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;

class AdminModule extends Module
{
    const DEFINITIONS = __DIR__ . '/config.php';

    public function __construct(RendererInterface $renderer, Router $router, string $prefix)
    {
        $renderer->addPath('admin', __DIR__ . '/views');

        $router->get($prefix, DashboardAction::class, 'admin');
        $router->get("$prefix/membres", AdminAction::class, 'admin.member');
        $router->post("$prefix/membres", AdminAction::class);
        $router->delete("$prefix/membres/{id}", AdminAction::class, 'admin.member.delete', ['id' => "\d+"]);
    }
}
