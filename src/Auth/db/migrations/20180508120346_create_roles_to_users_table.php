<?php


use Phinx\Migration\AbstractMigration;

class CreateRolesToUsersTable extends AbstractMigration
{

    public function change()
    {
        $this->table('user2role')
            ->addColumn('user_id', 'integer')
            ->addColumn('role_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id', ['delete' => 'CASCADE'])
            ->addForeignKey('role_id', 'roles', 'id', ['delete' => 'CASCADE'])
            ->create();
    }
}
