<?php

namespace Framework\Renderer;

/**
 * Class PHPRenderer
 * Permet de rendre des vue PHP (sans utiliser de moteur de template
 * @package Framework\Renderer
 */
class PHPRenderer implements RendererInterface
{
    /**
     * Namespace par défaut
     */
    const DEFAULT_NAMESPACE = '__MAIN';

    /**
     * Tableau de tout les chemins des vues
     * @var array
     */
    private $paths = [];

    /**
     * Tableau de toutes les variables globales ajoutées
     * @var array
     */
    private $globals = [];

    /**
     * PHPRenderer constructor.
     * @param null|string $defaultPath
     */
    public function __construct(?string $defaultPath = null)
    {
        if (!is_null($defaultPath)) {
            $this->addPath($defaultPath);
        }
    }

    /**
     * Change le namespace par défaut si il est définit
     * Ajoute le chemin de la vu à la clé namespace
     * @param string $namespace
     * @param null|string $path
     */
    public function addPath(string $namespace, ?string $path = null) : void
    {
        if (is_null($path)) {
            $this->paths[self::DEFAULT_NAMESPACE] = $namespace;
        } else {
            $this->paths[$namespace] = $path;
        }
    }

    /**
     * Rends une vue avec ces paramètres
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render(string $view, array $params = []) : string
    {
        if ($this->hasNamespace($view)) {
            $path = $this->replaceNamespace($view) . '.php';
        } else {
            $path = $this->paths[self::DEFAULT_NAMESPACE] . DIRECTORY_SEPARATOR . $view . '.php';
        }
        ob_start();
        $renderer = $this;
        extract($this->globals);
        extract($params);
        require($path);
        return ob_get_clean();
    }

    /**
     * Ajoute des variables global pour les rendre disponible à la vue
     * @param string $key
     * @param $value
     */
    public function addGlobal(string $key, $value) : void
    {
        $this->globals[$key] = $value;
    }

    /**
     * Retroune si une string contient un namespace ou non
     * @param string $view
     * @return bool
     */
    private function hasNamespace(string $view) : bool
    {
        return $view[0] === '@';
    }

    /**
     * Retourne le namespace
     * @param string $view
     * @return string
     */
    private function getNamespace(string $view) : string
    {
        return substr($view, 1, strpos($view, '/') - 1);
    }

    /**
     * Remplace le namespace dans une string
     * @param string $view
     * @return string
     */
    private function replaceNamespace(string $view) : string
    {
        $namespace = $this->getNamespace($view);
        return str_replace('@' . $namespace, $this->paths[$namespace], $view);
    }
}
