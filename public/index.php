<?php

/**
 * Entrée de l'applcation
 */

use App\Auth\ForbiddenMiddleware;
use App\Auth\RolesMiddleware;
use Framework\Auth\LoggedInMiddleware;
use Framework\Middleware\CsrfMiddleware;
use Framework\Middleware\DispatcherMiddleware;
use Framework\Middleware\MethodMiddleware;
use Framework\Middleware\NotFoundMiddleware;
use Framework\Middleware\RendererRequestMiddleware;
use Framework\Middleware\RouterMiddleware;
use Framework\Middleware\TrailingSlashMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use Middlewares\Whoops;

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

/* Initisalisation de l'application */
$app = (new \Framework\App('config/config.php'))
    ->addModule(\App\Landing\LandingModule::class)
    ->addModule(\App\Admin\AdminModule::class)
    ->addModule(\App\Auth\AuthModule::class)
    ->addModule(\App\Account\AccountModule::class)
    ->addModule(\App\Blog\BlogModule::class)
    ->addModule(\App\Club\ClubModule::class)
    ->addModule(\App\Product\ProductModule::class);

$container = $app->getContainer();
// MIDDLEWARES
$app->pipe(Whoops::class)
    ->pipe(TrailingSlashMiddleware::class)
    ->pipe(ForbiddenMiddleware::class)
    ->pipe($container->get('account.prefix'), LoggedInMiddleware::class)
    ->pipe(RolesMiddleware::class)
    ->pipe(MethodMiddleware::class)
    ->pipe(RendererRequestMiddleware::class) // Ajoute des variables global
    ->pipe(CsrfMiddleware::class)
    ->pipe(RouterMiddleware::class)
    ->pipe(DispatcherMiddleware::class)
    ->pipe(NotFoundMiddleware::class);

/* Lancement de l'application que si on est pas sur un terminal (pour eviter les conflit avec les migrations) */
if (php_sapi_name() !== "cli") {
    $response = $app->run(ServerRequest::fromGlobals());
    \Http\Response\send($response);
}
