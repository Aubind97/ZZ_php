<?php

namespace App\Landing;

use Framework\Module;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Psr\Container\ContainerInterface;

class LandingModule extends Module
{
    const DEFINITIONS = __DIR__ . '/config.php';

    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath('landing', __DIR__ . '/views');

        $router = $container->get(Router::class);
        $prefix = $container->get('landing.prefix');

        $router->get($prefix, LandingAction::class, 'landing.index');
    }
}
