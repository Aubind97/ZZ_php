<?php

namespace App\Blog;

use App\Blog\Actions\PostAdminAction;
use App\Blog\Actions\PostCrudAction;
use App\Blog\Actions\PostOwnerAction;
use App\Blog\Actions\PostOwnerAdminAction;
use Framework\Router;
use Framework\Module;
use App\Blog\Actions\BlogAction;
use Framework\Renderer\RendererInterface;
use Psr\Container\ContainerInterface;

class BlogModule extends Module
{

    const DEFINITIONS = __DIR__ . '/config.php';

    const MIGRATIONS = __DIR__ . '/db/migrations';

    const SEEDS = __DIR__ . '/db/seeds';

    /**
     * BlogModule constructor.
     * @param ContainerInterface $container
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $container->get(RendererInterface::class)->addPath('blog', __DIR__ . '/views');

        $router = $container->get(Router::class);
        $prefix = $container->get('blog.prefix');
        $router->get($prefix, BlogAction::class, 'blog.index');
        $router->get(
            $prefix . "/{slug}-{id}",
            BlogAction::class,
            'blog.show',
            [
                'slug' => '[a-z\-0-9]+',
                'id' => '[0-9]+'
            ]
        );

        if ($container->has('admin.prefix')) {
            $prefix = $container->get('admin.prefix');

            // CRUD
            $router->get("$prefix/posts", PostCrudAction::class, "blog.admin.index");
            $router->get("$prefix/posts/{id}", PostCrudAction::class, "blog.admin.edit", [
                "id" => "\d+"
            ]);
            $router->post("$prefix/posts/{id}", PostCrudAction::class, null, [
                "id" => "\d+"
            ]);
            $router->delete("$prefix/posts/{id}", PostCrudAction::class, "blog.admin.delete", [
                "id" => "\d+"
            ]);

            $router->post("$prefix/post/activer/{id}", PostAdminAction::class, 'blog.admin.activate', [
                'id' => '[0-9]+'
            ]);

            $router->get("/{slug}-admin/posts", PostOwnerAction::class, 'blog.owner.index', [
                'slug' => '[a-z]+'
            ]);

            $router->get("/{slug}-admin/posts/creer", PostOwnerAdminAction::class, 'blog.owner.create', [
                'slug' => '[a-z]+'
            ]);
            $router->post("/{slug}-admin/posts/creer", PostOwnerAdminAction::class, null, [
                'slug' => '[a-z]+'
            ]);
        }
    }
}
