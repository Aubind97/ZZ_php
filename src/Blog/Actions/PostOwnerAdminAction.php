<?php

namespace App\Blog\Actions;

use App\Blog\Table\PostTable;
use App\Club\Table\ClubTable;
use Framework\Actions\RouterAwareAction;
use Framework\Database\Hydrator;
use Framework\Renderer\RendererInterface;
use Framework\Router;
use Framework\Session\FlashService;
use Framework\Validator;
use Psr\Http\Message\ServerRequestInterface;

class PostOwnerAdminAction
{

    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var PostTable
     */
    private $postTable;
    /**
     * @var FlashService
     */
    private $flashService;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var ClubTable
     */
    private $clubTable;

    use RouterAwareAction;

    public function __construct(
        RendererInterface $renderer,
        PostTable $postTable,
        FlashService $flashService,
        Router $router,
        ClubTable $clubTable
    ) {
        $this->renderer = $renderer;
        $this->postTable = $postTable;
        $this->flashService = $flashService;
        $this->router = $router;
        $this->clubTable = $clubTable;
    }

    public function __invoke(ServerRequestInterface $request)
    {
        $slug = $request->getAttribute('slug');

        if ($request->getMethod() === 'POST') {
            return $this->create($request);
        }

        return $this->renderer->render('@blog/owner/posts/create', compact('slug'));
    }

    private function create(ServerRequestInterface $request)
    {
        $post = $this->postTable->getEntity();

        $slug = $request->getAttribute('slug');

        $validator = $this->getValidator($request);

        if ($validator->isValid()) {
            $params = $this->getParams($request);
            $this->postTable->insert($params);
            $this->flashService->success('Le post à bien été ajouté');
            return $this->redirect('club.owner.dashboard', ['slug' => $slug]);
        }
        Hydrator::hydrate($request->getParsedBody(), $post);
        $errors = $validator->getErrors();

        return $this->renderer->render('@blog/owner/posts/create', compact('slug', 'errors'));
    }

    /**
     * Filtre des paramètres particuliers
     * @param ServerRequestInterface $request
     * @return array
     * @throws \Framework\Database\NoRecordException
     */
    protected function getParams(ServerRequestInterface $request) : array
    {
        $params = array_filter($request->getParsedBody(), function ($key) {
            return in_array($key, [
                'name',
                'slug',
                'content',
                'created_at',
                'created_by'
            ]);
        }, ARRAY_FILTER_USE_KEY);

        $slug = $request->getAttribute('slug');
        $clubId = $this->clubTable->findBy('slug', $slug)->id;

        return array_merge($params, [
            'updated_at' => date('Y-m-d H:i:s'),
            'created_by' => $clubId
        ]);
    }

    protected function getValidator(ServerRequestInterface $request) : Validator
    {
        return (new Validator($request->getParsedBody()))
            ->required('content', 'name', 'slug', 'created_at')
            ->length('content', 10)
            ->length('name', 2, 250)
            ->length('slug', 2, 50)
            ->dateTime('created_at')
            ->slug('slug')
            ->unique('slug', $this->postTable, $this->postTable->getPdo(), true);
    }
}
