<?php

namespace App\Club\Entity;

class Club
{
    public $id;

    public $name;

    public $slug;

    public $description;

    public $shortDescription;

    public $createdAt;

    public $isActive;

    public $logo;

    public function setCreatedAt($datetime)
    {
        if (is_string($datetime)) {
            $this->createdAt = new \DateTime($datetime);
        } else {
            $this->createdAt = $datetime;
        }
    }

    public function getThumb()
    {
        $extension = pathinfo($this->logo, PATHINFO_EXTENSION);
        $filename = pathinfo($this->logo, PATHINFO_FILENAME);
        return '/uploads/clubs/' . $filename . '_thumb.' . $extension;
    }
}
