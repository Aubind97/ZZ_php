<?php

namespace App\Club;

use App\Club\Table\ClubTable;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

/**
 * Permet d'aller vers la gestion des clubs
 * Class ClubAdminWidget
 * @package App\Club
 */
class ClubAdminWidget implements WidgetInterface
{

    private $renderer;
    /**
     * @var ClubTable
     */
    private $clubTable;

    public function __construct(
        RendererInterface $renderer,
        ClubTable $clubTable
    ) {
        $this->renderer = $renderer;
        $this->clubTable = $clubTable;
    }

    public function render(): string
    {
        $activateClubs = $this->clubTable->getActiveClubsNumber();
        $inactivateClubs = $this->clubTable->getInactiveClubsNumber();
        return $this->renderer->render('@club/admin/clubs/widget', compact('activateClubs', 'inactivateClubs'));
    }
}
