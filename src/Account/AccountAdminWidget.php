<?php

namespace App\Account;

use App\Auth\UserTable;
use Framework\Renderer\RendererInterface;
use Framework\WidgetInterface;

class AccountAdminWidget implements WidgetInterface
{

    private $renderer;
    /**
     * @var UserTable
     */
    private $userTable;

    public function __construct(RendererInterface $renderer, UserTable $userTable)
    {
        $this->renderer = $renderer;
        $this->userTable = $userTable;
    }

    public function render(): string
    {
        $users = $this->userTable->getMembersNumber();
        return $this->renderer->render('@account/admin/widget', compact('users'));
    }
}
