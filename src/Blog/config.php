<?php

use App\Blog\BlogWidget;

/**
 * Configuration des dépendances du module pour le conteneur d'injection
 */
return [
    'blog.prefix' => '/news',
    'admin.widgets' => \DI\add([
        \DI\get(BlogWidget::class)
    ]),

    'public.nav' => \DI\add([
        'news' => [
            'link' => '/news',
            'name' => 'News'
        ]
    ])
];
