<?php

namespace App\Landing;

use Framework\Renderer\RendererInterface;
use Framework\Router;

class LandingAction
{
    /**
     * @var RendererInterface
     */
    private $renderer;
    /**
     * @var Router
     */
    private $router;

    public function __construct(RendererInterface $renderer, Router $router)
    {
        $this->renderer = $renderer;
        $this->router = $router;
    }

    public function __invoke() : string
    {
        return $this->renderer->render('@landing/index');
    }
}
