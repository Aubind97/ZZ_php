<?php

namespace Tests\Framework\Middleware;

use Framework\Middleware\NotFoundMiddleware;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;

class NotFoundMiddlewareTest extends TestCase
{
    public function testSendNotFound()
    {
        $response = new ServerRequest('GET', '/jenexistepas');
        $middleware = new NotFoundMiddleware();
        $response = call_user_func_array($middleware, [$response, function () {
        }]);
        $this->assertEquals(404, $response->getStatusCode());
    }
}
