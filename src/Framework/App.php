<?php

namespace Framework;

use DI\Container;
use DI\ContainerBuilder;
use Framework\Middleware\CombinedMiddleware;
use Framework\Middleware\RoutePrefixedMiddleware;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class App
 * @package Framework
 */
class App implements RequestHandlerInterface
{
    /**
     * Tableau des modules utilisés dans l'application
     * @var array
     */
    private $modules = [];

    private $definition;

    private $container;

    private $middlewares;

    private $index = 0;

    public function __construct(string $definition)
    {
        $this->definition = $definition;
    }

    /**
     * Rajoute un modules a l'application
     * @param string $module
     * @return App
     */
    public function addModule(string $module) : self
    {
        $this->modules[] = $module;
        return $this;
    }

    /**
     * Ajoute un middlewares a l'application
     * @param string|callable|MiddlewareInterface $routePrefix
     * @param string|callable|MiddlewareInterface $middleware
     * @return App
     * @throws \Exception
     */
    public function pipe($routePrefix, $middleware = null) : self
    {
        if ($middleware === null) {
            $this->middlewares[] = $routePrefix;
        } else {
            $this->middlewares[] = new RoutePrefixedMiddleware($this->getContainer(), $routePrefix, $middleware);
        }
        return $this;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     */
    public function handle(ServerRequestInterface $request) : ResponseInterface
    {
        $this->index++;
        if ($this->index > 1) {
            throw new \Exception();
        }
        $middleware = new CombinedMiddleware($this->getContainer(), $this->middlewares);
        return $middleware->process($request, $this);
    }

    /**
     * Lance l'application
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws \Exception
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function run(ServerRequestInterface $request): ResponseInterface
    {
        foreach ($this->modules as $module) {
            $this->getContainer()->get($module);
        }

        return $this->handle($request);
    }

    /**
     * Retourne le conteneur d'injection de dep.
     * @return Container
     * @throws \Exception
     */
    public function getContainer() : ContainerInterface
    {
        if ($this->container === null) {
            $builder = new ContainerBuilder();

            // TODO : faire la mise en cache

             $builder->addDefinitions($this->definition);
            foreach ($this->modules as $module) {
                if ($module::DEFINITIONS) {
                    $builder->addDefinitions($module::DEFINITIONS);
                }
            }
            $this->container = $builder->build();
        }
        return $this->container;
    }

    public function getModules() : array
    {
        return $this->modules;
    }
}
