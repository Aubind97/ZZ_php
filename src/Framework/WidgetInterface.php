<?php

namespace Framework;

interface WidgetInterface
{
    public function render() : string;
}
